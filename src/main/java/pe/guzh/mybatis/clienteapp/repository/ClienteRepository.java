package pe.guzh.mybatis.clienteapp.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import pe.guzh.mybatis.clienteapp.model.Cliente;

@Mapper
public interface ClienteRepository {

    @Select("SELECT * FROM tb_cliente order by id")
    List<Cliente> findAll();

    @Select("SELECT * FROM tb_cliente WHERE id = #{id}")
    Cliente findById(int id);

    @Insert("INSERT INTO tb_cliente (nombre, apellido, correo, balance) VALUES (#{nombre}, #{apellido}, #{correo}, #{balance})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    Integer insert(Cliente cliente);

    @Update("UPDATE tb_cliente SET nombre = #{nombre}, apellido = #{apellido}, correo = #{correo}, balance = #{balance} WHERE id = #{id}")
    Integer update(Cliente cliente);

    @Delete("DELETE FROM tb_cliente WHERE id = #{id}")
    void delete(int id);
}