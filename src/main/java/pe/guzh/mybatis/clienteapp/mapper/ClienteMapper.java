package pe.guzh.mybatis.clienteapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import pe.guzh.mybatis.clienteapp.dto.ClienteDto;
import pe.guzh.mybatis.clienteapp.model.Cliente;

@Mapper
public interface ClienteMapper {
    ClienteMapper INSTANCE = Mappers.getMapper(ClienteMapper.class);

    ClienteDto clienteToClienteDTO(Cliente cliente);
    Cliente clienteDTOToCliente(ClienteDto clienteDTO);
}