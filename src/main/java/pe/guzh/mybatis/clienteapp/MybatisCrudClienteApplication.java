package pe.guzh.mybatis.clienteapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisCrudClienteApplication {

	public static void main(String[] args) {
		SpringApplication.run(MybatisCrudClienteApplication.class, args);
	}

}
