package pe.guzh.mybatis.clienteapp.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import pe.guzh.mybatis.clienteapp.dto.ResponseDto;

public class HttpReponseUtil<T> {
	public static <T> ResponseEntity<ResponseDto<T>> createResponse(List<T> data, String mensaje, HttpStatus estado){
		ResponseDto<T> responseDto = new ResponseDto<>();
		responseDto.setCodigo(estado.value());
		responseDto.setMensaje(mensaje);
		responseDto.setData(data);
		
		return new ResponseEntity<>(responseDto, estado);
	}
	
	public static <T> ResponseEntity<ResponseDto<T>> createResponse(T data, String mensaje, HttpStatus estado){
		ResponseDto<T> responseDto = new ResponseDto<>();
		responseDto.setCodigo(estado.value());
		responseDto.setMensaje(mensaje);
		List<T> lista = new ArrayList<>();
		lista.add(data);
		responseDto.setData(lista);
		
		return new ResponseEntity<>(responseDto, estado);
	}

}
