package pe.guzh.mybatis.clienteapp.dto;

import java.math.BigDecimal;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class ClienteDto {
    private Integer id;

    @NotBlank(message = "El nombre es obligatorio")
    @Size(max = 60, message = "El nombre no debe exceder los 60 caracteres")
    private String nombre;

    @NotBlank(message = "El apellido es obligatorio")
    @Size(max = 60, message = "El apellido no debe exceder los 60 caracteres")
    private String apellido;

    @Email(message = "Correo electrónico inválido")
    @NotBlank(message = "El correo es obligatorio")
    @Size(max = 80, message = "El correo no debe exceder los 80 caracteres")
    private String correo;

    @NotNull(message = "El balance es obligatorio")
    private BigDecimal balance;
}