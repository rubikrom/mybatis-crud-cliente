package pe.guzh.mybatis.clienteapp.dto;

import java.util.List;

import lombok.Data;

@Data
public class ResponseDto<T> {
	private Integer codigo;
	private String mensaje;
	private List<T> data;
}
