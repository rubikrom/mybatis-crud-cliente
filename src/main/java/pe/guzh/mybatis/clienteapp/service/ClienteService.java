package pe.guzh.mybatis.clienteapp.service;

import java.util.List;

import pe.guzh.mybatis.clienteapp.dto.ClienteDto;

public interface ClienteService {
	
	/**
	 * Devuelve todos los clientes
	 * 
	 * @return List<ClienteDto>
	 */
	public List<ClienteDto> obtenerTodos();
	/**
	 * Devuelve un cliente
	 * 
	 * @param id, es un numero entero que identifica al cliente
	 * @return ClienteDto
	 */
	public ClienteDto obtenerPorId(int id);
	/**
	 * Agrega un cliente a la base de datos
	 * 
	 * @param clienteDTO
	 */
	public ClienteDto crear(ClienteDto clienteDTO);
	/**
	 * Actualiza los datos del cliente
	 * 
	 * @param id
	 * @param clienteDTO
	 */
	public ClienteDto actualizar(int id, ClienteDto clienteDTO);
	/**
	 * Elimina los datos de un cliente
	 * 
	 * @param id
	 */
	public void eliminar(int id);
	
}
