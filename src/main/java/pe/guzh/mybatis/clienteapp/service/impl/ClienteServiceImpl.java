package pe.guzh.mybatis.clienteapp.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.mybatis.spring.MyBatisSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.guzh.mybatis.clienteapp.dto.ClienteDto;
import pe.guzh.mybatis.clienteapp.mapper.ClienteMapper;
import pe.guzh.mybatis.clienteapp.model.Cliente;
import pe.guzh.mybatis.clienteapp.repository.ClienteRepository;
import pe.guzh.mybatis.clienteapp.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	private final ClienteMapper clienteMapper = ClienteMapper.INSTANCE;

	public List<ClienteDto> obtenerTodos() throws MyBatisSystemException {
		return clienteRepository.findAll().stream().map(clienteMapper::clienteToClienteDTO)
				.collect(Collectors.toList());
	}

	public ClienteDto obtenerPorId(int id) throws MyBatisSystemException {
		Cliente cliente = clienteRepository.findById(id);
		return clienteMapper.clienteToClienteDTO(cliente);
	}

	public ClienteDto crear(ClienteDto clienteDto) throws MyBatisSystemException {
		Cliente cliente = clienteMapper.clienteDTOToCliente(clienteDto);
		clienteRepository.insert(cliente);

		return clienteMapper.clienteToClienteDTO(cliente);
	}

	public ClienteDto actualizar(int id, ClienteDto clienteDto) throws MyBatisSystemException {
		Cliente cliente = clienteMapper.clienteDTOToCliente(clienteDto);
		cliente.setId(id);
		clienteRepository.update(cliente);

		return clienteMapper.clienteToClienteDTO(cliente);
	}

	public void eliminar(int id) throws MyBatisSystemException {
		clienteRepository.delete(id);
	}
}