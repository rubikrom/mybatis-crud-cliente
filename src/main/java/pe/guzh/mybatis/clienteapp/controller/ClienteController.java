package pe.guzh.mybatis.clienteapp.controller;

import java.util.List;

import org.mybatis.spring.MyBatisSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;
import pe.guzh.mybatis.clienteapp.dto.ClienteDto;
import pe.guzh.mybatis.clienteapp.dto.ResponseDto;
import pe.guzh.mybatis.clienteapp.service.ClienteService;
import pe.guzh.mybatis.clienteapp.util.HttpReponseUtil;

@RestController
@RequestMapping("/api/clientes")
@Validated
public class ClienteController {

	@Autowired
    private ClienteService clienteService;

    @GetMapping
    public ResponseEntity<ResponseDto<ClienteDto>> obtenerClientes() {
    	String mensaje = null;
    	
    	try {
    		List<ClienteDto> clienteDtoLista = clienteService.obtenerTodos();
    		mensaje = !CollectionUtils.isEmpty(clienteDtoLista) ? "Clientes encontrados: " + clienteDtoLista.size() : "Sin datos";
    		return HttpReponseUtil.createResponse(clienteDtoLista, mensaje, HttpStatus.OK);
    	} catch (MyBatisSystemException e) {
    		mensaje = "Error: " + e.getCause();
    		return HttpReponseUtil.createResponse(null, mensaje, HttpStatus.INTERNAL_SERVER_ERROR);
    	}
    	
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDto<ClienteDto>> obtenerCliente(@PathVariable int id) {
    	String mensaje = null;
    	try {
    		ClienteDto clienteDto = clienteService.obtenerPorId(id);
    		mensaje = clienteDto != null ? "Cliente encontrado" : "Sin datos";
    		return HttpReponseUtil.createResponse(clienteDto, mensaje, HttpStatus.OK);
    	} catch (MyBatisSystemException e) {
			mensaje = "Error: " + e.getCause();
			return HttpReponseUtil.createResponse(null, mensaje, HttpStatus.INTERNAL_SERVER_ERROR);
		}
        
    }

    @PostMapping
    public ResponseEntity<ResponseDto<ClienteDto>> crear(@Valid @RequestBody ClienteDto clienteDto) {
    	String mensaje = null;
    	try {
        	ClienteDto clienteDtoNuevo = clienteService.crear(clienteDto);
        	mensaje = clienteDtoNuevo != null ? "Cliente encontrado" : "Sin datos";
        	return HttpReponseUtil.createResponse(clienteDtoNuevo, "Cliente agregado", HttpStatus.OK);
        } catch (MyBatisSystemException e) {
			System.out.println(e);
			mensaje = "Error: " + e.getCause();
			return HttpReponseUtil.createResponse(null, mensaje, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseDto<ClienteDto>> actualizar(@PathVariable int id, @Valid @RequestBody ClienteDto clienteDto) {
    	String mensaje = null;
    	try {
			ClienteDto clienteDtoNuevo = clienteService.actualizar(id, clienteDto);
			mensaje = clienteDtoNuevo != null ? "Cliente encontrado" : "Sin datos";
			return HttpReponseUtil.createResponse(clienteDtoNuevo, "Cliente actualizado", HttpStatus.OK);
		} catch (MyBatisSystemException e) {
			mensaje = "Error: " + e.getCause();
			return HttpReponseUtil.createResponse(null, mensaje, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDto<ClienteDto>> eliminar(@PathVariable int id) {
    	String mensaje = null;
    	try {
    		clienteService.eliminar(id);
    		return HttpReponseUtil.createResponse(null, "Cliente eliminado", HttpStatus.NO_CONTENT);
    	} catch (MyBatisSystemException e) {
			mensaje = "Error: " + e.getCause();
			return HttpReponseUtil.createResponse(null, mensaje, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
}