package pe.guzh.mybatis.clienteapp.model;

import lombok.Data;

@Data
public class Cliente {
    private Integer id;
    private String nombre;
    private String apellido;
    private String correo;
    private Double balance;
}
